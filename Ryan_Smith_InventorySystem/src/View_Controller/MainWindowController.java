package View_Controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class MainWindowController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button addPart;

    @FXML
    private Button addProduct;

    @FXML
    private Button deletePart;

    @FXML
    private Button deleteProduct;

    @FXML
    private Button exitMain;

    @FXML
    private Button modifyPart;

    @FXML
    private Button modifyProduct;

    @FXML
    private TableColumn<?, ?> partColID;

    @FXML
    private TableColumn<?, ?> partColInv;

    @FXML
    private TableColumn<?, ?> partColName;

    @FXML
    private TableColumn<?, ?> partColPrice;

    @FXML
    private TableView<?> partTable;

    @FXML
    private TableColumn<?, ?> productColID;

    @FXML
    private TableColumn<?, ?> productColInv;

    @FXML
    private TableColumn<?, ?> productColName;

    @FXML
    private TableColumn<?, ?> productColPrice;

    @FXML
    private AnchorPane productTable;

    @FXML
    private TextField searchPart;

    @FXML
    private TextField searchProduct;

    @FXML
    void initialize() {
        assert addPart != null : "fx:id=\"addPart\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert addProduct != null : "fx:id=\"addProduct\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert deletePart != null : "fx:id=\"deletePart\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert deleteProduct != null : "fx:id=\"deleteProduct\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert exitMain != null : "fx:id=\"exitMain\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert modifyPart != null : "fx:id=\"modifyPart\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert modifyProduct != null : "fx:id=\"modifyProduct\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert partColID != null : "fx:id=\"partColID\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert partColInv != null : "fx:id=\"partColInv\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert partColName != null : "fx:id=\"partColName\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert partColPrice != null : "fx:id=\"partColPrice\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert partTable != null : "fx:id=\"partTable\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert productColID != null : "fx:id=\"productColID\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert productColInv != null : "fx:id=\"productColInv\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert productColName != null : "fx:id=\"productColName\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert productColPrice != null : "fx:id=\"productColPrice\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert productTable != null : "fx:id=\"productTable\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert searchPart != null : "fx:id=\"searchPart\" was not injected: check your FXML file 'MainWindow.fxml'.";
        assert searchProduct != null : "fx:id=\"searchProduct\" was not injected: check your FXML file 'MainWindow.fxml'.";

    }

}
