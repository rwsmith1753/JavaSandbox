package Model;

public class In_House extends Part {

    private int machineID;
    public In_House(int id, String name, double price, int stock, int min, int max, int machineID) {
        super(id, name, price, stock, min, max);

        this.machineID = machineID;
    }

    public int getMachineID() {
        return machineID;
    }

    public void setMachineID() {
        this.machineID = machineID;
    }
}
