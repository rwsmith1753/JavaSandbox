package Model;

public abstract class Product {
    private int id;
    private String name;
    private double price;
    private int stock;
    private int min;
    private int max;

    public Product(int id, String name, double price, int stock, int min, int max) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.min = min;
        this.max = max;
//        this.associatedParts = associatedParts;
    }

    public int getId() {return id;}
    public String getName() {return name;}
    public double getPrice() {return price;}
    public int getStock() {return stock;}
    public int getMin() {return min;}
    public int getMax() {return max;}

    public void setId() {this.id = id;}
    public void setName() {this.name = name;}
    public void setPrice() {this.price = price;}
    public void setStock() {this.stock = stock;}
    public void setMin() {this.min = min;}
    public void setMax() {this.max = max;}

    public void addAssociatedPart(Part part) {

    }

    public boolean deleteAssociatedPart(Part selectedAssociatedPart) {
        return true;
    }

//    getAllAssociatedParts(){}
}
